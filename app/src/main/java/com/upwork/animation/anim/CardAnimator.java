package com.upwork.animation.anim;

import android.animation.ValueAnimator;
import android.graphics.Point;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;

public class CardAnimator {

    private int ANIM_DURATION = 5000;

    private float                          r;
    private float                          d_angle;
    private Point                          mCenter;
    private AnimableCards                  mAnimableCards;
    private ArrayList<ArrayList<AnimInfo>> mFinalyPositions;

    public CardAnimator(AnimableCards animableCards) {
        mAnimableCards = animableCards;

        View parent = animableCards.getParentView();
        int width = parent.getWidth() - parent.getPaddingLeft() -
                parent.getPaddingRight() - animableCards.getPaddings();
        int height = parent.getHeight() - parent.getPaddingTop() -
                parent.getPaddingBottom() - animableCards.getPaddings();
        mCenter = new Point(parent.getWidth() / 2, parent.getHeight() / 2);
        r = Math.min(width / 2f, height / 2f);
        d_angle = 360 / (2 * animableCards.getCards().get(0).size() - 1);
    }

    public void startAnimation() {
        ArrayList<ArrayList<View>> cards = mAnimableCards.getCards();
        mFinalyPositions = new ArrayList<>(cards.size());
        int j = 0;
        ArrayList<AnimInfo> array = new ArrayList<>(cards.get(0).size());
        for (int i = 0; i < cards.get(0).size(); i++) {
            float angle = j * d_angle;
            array.add(getAnimInfo(angle, cards.get(0).get(i)));
            j++;
        }
        mFinalyPositions.add(array);

        array = new ArrayList<>(cards.get(2).size());
        for (int i = 0; i < cards.get(2).size(); i++) {
            float angle = j * d_angle;
            array.add(getAnimInfo(angle, cards.get(2).get(i)));
            j++;
        }
        mFinalyPositions.add(array);

        animate();
    }

    private void animate() {
        final ArrayList<ArrayList<View>> cards = mAnimableCards.getCards();
        final float endValue = mFinalyPositions.get(0).get(0).finalRotation;
        ValueAnimator va = ValueAnimator.ofFloat(0, endValue);
        va.setDuration(ANIM_DURATION);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {

                View parent = mAnimableCards.getParentView();

                for (int i = 0; i < cards.get(0).size(); i++) {
                    AnimInfo fp = mFinalyPositions.get(0).get(i);

                    float a = ((float) animation.getAnimatedValue()) / endValue;

                    View card = cards.get(0).get(i);
                    RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(card.getWidth(), card.getHeight());
                    lp.leftMargin = Math.round((fp.finalPosition.x - fp.startPosition.x) * a)
                            + fp.startPosition.x - parent.getPaddingLeft();
                    lp.topMargin = Math.round((fp.finalPosition.y - fp.startPosition.y) * a)
                            + fp.startPosition.y - parent.getPaddingTop();
                    card.setLayoutParams(lp);
                    card.setRotation(fp.finalRotation * a);
                }

                for (int i = 0; i < cards.get(2).size(); i++) {
                    AnimInfo fp = mFinalyPositions.get(1).get(i);

                    float a = ((float) animation.getAnimatedValue()) / endValue;

                    View card = cards.get(2).get(i);
                    RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(card.getWidth(), card.getHeight());
                    lp.leftMargin = Math.round((fp.finalPosition.x - fp.startPosition.x) * a)
                            + fp.startPosition.x - parent.getPaddingLeft();
                    lp.topMargin = Math.round((fp.finalPosition.y - fp.startPosition.y) * a)
                            + fp.startPosition.y - parent.getPaddingTop();
                    card.setLayoutParams(lp);
                    card.setRotation(fp.finalRotation * a);
                }
            }
        });
        va.start();
    }

    private AnimInfo getAnimInfo(float angle, View card) {
        double rads = Math.toRadians(angle);
        float sin = (float) Math.sin(rads);
        float cos = (float) Math.cos(rads);
        int dx = Math.round(mCenter.x - r * sin);
        int dy = Math.round(mCenter.y - r * cos);
        AnimInfo result = new AnimInfo();
        result.finalRotation = 90 - angle;
        result.startPosition = new Point(card.getLeft(), card.getTop());
        result.finalPosition = new Point(dx - card.getWidth() / 2, dy - card.getHeight() / 2);
        return result;
    }

    private class AnimInfo {
        float finalRotation;
        Point startPosition;
        Point finalPosition;
    }
}
