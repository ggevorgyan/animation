package com.upwork.animation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.upwork.animation.anim.AnimableCards;
import com.upwork.animation.anim.CardAnimator;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AnimableCards {

    private ArrayList<ArrayList<View>> mCards = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RelativeLayout root = (RelativeLayout) findViewById(R.id.root);
        ImageView imageView;
        ArrayList<View> array;

        int height = 120;
        int width = 90;

        int id = 120;
        array = new ArrayList<>();
        for (int i = 0; i < 13; i++) {
            imageView = new ImageView(this);
            imageView.setBackgroundResource(R.drawable.bckg);
            imageView.setImageResource(R.drawable.heart);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(width, height);
            lp.setMargins(2, 2, 2, 2);
            array.add(imageView);
            imageView.setPadding(2, 2, 2, 2);
            root.addView(imageView, lp);
            imageView.setId(id + i);
        }
        mCards.add(array);

        int id1 = id + 1;
        array = new ArrayList<>();
//        for (int i = 0; i < 13; i++) {
//            imageView = new ImageView(this);
//            imageView.setImageResource(R.drawable.club);
//            imageView.setBackgroundResource(R.drawable.bckg);
//            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(width, height);
//            lp.addRule(RelativeLayout.RIGHT_OF, id);
//            lp.setMargins(2, 2, 2, 2);
//            imageView.setId(id1 + i);
//            imageView.setPadding(2, 2, 2, 2);
//            array.add(imageView);
//            root.addView(imageView, lp);
//        }
        mCards.add(array);

        array = new ArrayList<>();
        int id2 = id1 + 1;
        for (int i = 0; i < 13; i++) {
            imageView = new ImageView(this);
            imageView.setImageResource(R.drawable.diamond);
            imageView.setBackgroundResource(R.drawable.bckg);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(width, height);
            lp.addRule(RelativeLayout.RIGHT_OF, id1);
            lp.setMargins(2, 2, 2, 2);
            imageView.setId(id2 + i);
            imageView.setPadding(2, 2, 2, 2);
            array.add(imageView);
            root.addView(imageView, lp);
        }
        mCards.add(array);

        array = new ArrayList<>();
        int id3 = id2 + 1;
        for (int i = 0; i < 13; i++) {
//            imageView = new ImageView(this);
//            imageView.setImageResource(R.drawable.spade);
//            imageView.setBackgroundResource(R.drawable.bckg);
//            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(width, height);
//            lp.addRule(RelativeLayout.RIGHT_OF, id2);
//            lp.setMargins(2, 2, 2, 2);
//            imageView.setId(id3 + i);
//            imageView.setPadding(2, 2, 2, 2);
//            array.add(imageView);
//            root.addView(imageView, lp);
        }
        mCards.add(array);

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CardAnimator ca = new CardAnimator(MainActivity.this);
                ca.startAnimation();
            }
        });
    }

    @Override
    public ArrayList<ArrayList<View>> getCards() {
        return mCards;
    }

    @Override
    public ViewGroup getParentView() {
        return (RelativeLayout) findViewById(R.id.root);
    }

    @Override
    public int getAnimationDuration() {
        return 4000;
    }

    @Override
    public int getPaddings() {
        return 270;
    }
}
