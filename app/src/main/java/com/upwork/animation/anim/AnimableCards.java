package com.upwork.animation.anim;

import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public interface AnimableCards {

    ArrayList<ArrayList<View>> getCards();

    ViewGroup getParentView();

    int getPaddings();

    int getAnimationDuration();
}
